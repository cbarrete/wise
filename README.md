This is an short exercise in using both plain Python and Sage to explore group
theory.

We first construct the Cayley tables for groups $`(\mathbb{Z}/4\mathbb{Z}, +,
0)`$ and $`(\mathbb{Z}/2\mathbb{Z} \times \mathbb{Z}/2\mathbb{Z}, +, 0)`$,
verify and extract some properties about them.

We then search their automorphisms and isomorphisms, verify one of Lagrange's
theorems and search the minimal latin square not corresponding to a Cayley
table.
